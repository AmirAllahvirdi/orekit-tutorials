![Orekit logo](https://www.orekit.org/img/orekit-logo.png)

# Orekit tutorials

This project contains tutorials for the [Orekit](https://www.orekit.org) space dynamics library.

## Download

### Official releases

[Official Orekit tutorial releases](https://gitlab.orekit.org/orekit/orekit-tutorials/releases)
are available on our [Gitlab instance](https://gitlab.orekit.org/).

### Development version

To get the latest development version, please clone our official repository
and checkout the `develop` branch:

```bash
git clone -b develop https://gitlab.orekit.org/orekit/orekit-tutorials.git
```

## Getting help

The main communication channel is our [forum](https://forum.orekit.org/). You
can report bugs and suggest new features in our
[issues tracking system](https://gitlab.orekit.org/orekit/orekit-tutorials/issues). When
reporting security issues check the "This issue is confidential" box.

## Dependencies

Orekit tutorials relies on the following
[FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software) libraries,
all released under business friendly FOSS licenses.

### Compile-time/run-time dependencies

* [Orekit](https://www.orekit.org/), a space flight dynamics library released under
  the Apache License, version 2.0.

* [Hipparchus](https://hipparchus.org/), a mathematics library released under
  the Apache License, version 2.0.

### Test-time dependencies

* [JUnit 4](http://www.junit.org/), a widely used unit test framework released
  under the Eclipse Public License, version 1.0.

* [Mockito](https://site.mockito.org/), a mocking framework for unit tests,
  released under MIT license.

## License

Orekit tutorials are licensed by [CS GROUP](https://www.c-s.fr/) under
the [Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).
A copy of this license is provided in the [LICENSE.txt](LICENSE.txt) file.
